import requests, warnings
warnings.filterwarnings("ignore")

response = requests.get("http://localhost:5000")
assert response.status_code == 200
assert response.text.lower().index("weather") > 0