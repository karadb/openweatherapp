"""Weather Forecast App"""

from flask import Flask, render_template as rt
import requests, warnings

app = Flask(__name__)
title = "Weather Site"
bnr = "Weather Site"
ftr = "See you tomorrow!"


@app.route("/")
@app.route("/home")
def get_weather():
    lt = 40.7608
    ln = -111.8910
    k = "3c9bc7bc5f01ff49317437297efac00e"
    u = f"https://api.openweathermap.org/data/2.5/weather?lat={lt}&lon={ln}&appid={k}"
    response = requests.api.get(u, timeout=1).json()
    weather_response = response["weather"]
    weather_description = weather_response[0]["description"]
    return rt(
        "home.html", title=title, banner=bnr, content=weather_description, footer=ftr
    )


if __name__ == "__main__":
    app.run()
